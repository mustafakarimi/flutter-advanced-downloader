import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_advanced_downloader_example/models/list_of_downloads_info.dart';
import 'package:permission_handler/permission_handler.dart';

class MethodChannelHandler {
  static const METHOD_CHANNEL_NAME = "downloader/method_call";
  static const EVENT_CHANNEL_NAME = "downloader/event_call";

  MethodChannel _methodChannel = MethodChannel(METHOD_CHANNEL_NAME);
  EventChannel _eventChannel = EventChannel(EVENT_CHANNEL_NAME);

  Future<void> registerListener(StreamController<dynamic> stream) async {
    _eventChannel.receiveBroadcastStream().listen((event) {
      stream.add(event);
    });
    await _registerListener();
  }

  Future<void> startDownloadService({String url,
    String savePath }) async {

    if(savePath == null)
      savePath ="file:///storage/emulated/0/Download";
    var storage = await Permission.storage.isGranted;
    if (!storage) {
      await Permission.storage.request();
    }
    await _methodChannel
        .invokeMethod('start_download', {"url": url, "path": savePath});
  }

  Future<void> _registerListener() async {
    await _methodChannel.invokeMethod('register_listener');
  }

  Future<void> changeBaseUrl(String uuid, String url) async {
    await _methodChannel.invokeMethod(
      'change_base_url',
      {"uuid": uuid, "url": url},
    );
  }

  Future<ListOfDownloadsInfo> getAllDownloadsInfo() async {
    var res = await _methodChannel.invokeMethod("getAllDownloadsInfo");
    if (res == null || res == "null")
      return null;
    return ListOfDownloadsInfo.fromJson(jsonDecode(res));
  }

  Future<Info> getDownloadById(String uuid) async {
    return await _methodChannel.invokeMethod(
        "getDownloadInfoById", {"id": uuid});
  }

  Future<void> deleteDownloadInfoById(String uuid, bool withFile) async {
    return await _methodChannel.invokeMethod(
        "deleteDownloadInfoById", {"id": uuid, "withFile": withFile});
  }

  Future<void> pauseAllDownloads() async {
    return await _methodChannel.invokeMethod("pauseAllDownloads");
  }

  Future<void> stopAllDownloads() async {
    return await _methodChannel.invokeMethod("stopAllDownloads");
  }

  Future<void> resumeOrPauseDownloadByID(String uuid) async {
    return await _methodChannel.invokeMethod(
        "resumeOrPauseDownloadByID", {"id": uuid});
  }

  Future<dynamic> getAllDownloadsPieceAndInfo() async {
    return await _methodChannel.invokeMethod("getAllDownloadsPieceAndInfo");
  }
}



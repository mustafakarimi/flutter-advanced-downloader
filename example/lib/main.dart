import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_advanced_downloader_example/providers/download_manager.provider.dart';
import 'package:provider/provider.dart';

import 'core/method_channel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => DownloadManagerProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'downloader'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final silentProvider =
        Provider.of<DownloadManagerProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                silentProvider.addNewDownload(
                    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4");
              }),
          IconButton(
              icon: Icon(Icons.stop),
              onPressed: () async {
                silentProvider.stopAll();
              }),
          IconButton(
              icon: Icon(Icons.pause),
              onPressed: () async {
                silentProvider.pauseAll();
              }),
          IconButton(
              icon: Icon(Icons.bug_report),
              onPressed: () async {
                silentProvider.getAllDownloads();
              }),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: Consumer<DownloadManagerProvider>(
                  builder: (ctx, snapshot, child) {
                if (snapshot.allDownloads.isEmpty) {
                  return Container(
                    child: Center(
                      child: Text("No Downloads"),
                    ),
                  );
                }
                return ListView(
                  children: <Widget>[
                    ...snapshot.allDownloads.map(
                      (e) => GestureDetector(
                        onDoubleTap: () {
                          MethodChannelHandler().changeBaseUrl(e.id,
                              "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4");
                        },
                        child: ListTile(
                          title: Text(e.fileName),
                          leading: Icon(Icons.download_outlined),
                          trailing: Text(e.status),
                          onTap: () {
                            snapshot.pauseOrResumeDownload(e.id);
                          },
                          onLongPress: () {
                            snapshot.deleteDownloadByID(e.id);
                          },
                          subtitle: Text(
                            (e.piecesInfo[0].curBytes / 1024 / 1024)
                                    .toStringAsFixed(2) +
                                "MB " +
                                " - " +
                                (e.piecesInfo[0].speed / 1024)
                                    .toStringAsFixed(2) +
                                "Kb/s",
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                child: Text(
              "items action: \n long tap: delete , one tap: pause/resume \n double tap: change base url",
              textAlign: TextAlign.center,
            )),
          )
        ],
      ),
    );
  }
}

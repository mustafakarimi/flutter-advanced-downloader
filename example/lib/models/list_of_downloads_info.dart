class ListOfDownloadsInfo {
  List<Info> info;

  ListOfDownloadsInfo({this.info});

  ListOfDownloadsInfo.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<Info>();
      json['info'].forEach((v) {
        info.add(new Info.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Info {
  int numFailed;
  String fileName;
  int lastModify;
  int numPieces;
  String description;
  int totalBytes;
  String dirPath;
  String id;
  String url;
  String status;

  Info(
      {this.numFailed,
        this.fileName,
        this.lastModify,
        this.numPieces,
        this.description,
        this.totalBytes,
        this.dirPath,
        this.id,
        this.url,
        this.status});

  Info.fromJson(Map<String, dynamic> json) {
    numFailed = json['numFailed'];
    fileName = json['fileName'];
    lastModify = json['lastModify'];
    numPieces = json['numPieces'];
    description = json['description'];
    totalBytes = json['totalBytes'];
    dirPath = json['dirPath'];
    id = json['id'];
    url = json['url'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numFailed'] = this.numFailed;
    data['fileName'] = this.fileName;
    data['lastModify'] = this.lastModify;
    data['numPieces'] = this.numPieces;
    data['description'] = this.description;
    data['totalBytes'] = this.totalBytes;
    data['dirPath'] = this.dirPath;
    data['id'] = this.id;
    data['url'] = this.url;
    data['status'] = this.status;
    return data;
  }
}

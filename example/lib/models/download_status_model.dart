class DownloadStatusModel {
  int numFailed;
  List<PiecesInfo> piecesInfo;
  String fileName;
  int lastModify;
  int numPieces;
  String description;
  int totalBytes;
  String dirPath;
  String id;
  String event;
  String url;
  String status;

  DownloadStatusModel(
      {this.numFailed,
      this.piecesInfo,
      this.fileName,
      this.lastModify,
      this.numPieces,
      this.description,
      this.totalBytes,
      this.dirPath,
      this.id,
      this.event,
      this.url});

  DownloadStatusModel.fromJson(Map<String, dynamic> json) {
    numFailed = json['numFailed'];
    if (json['piecesInfo'] != null) {
      piecesInfo = new List<PiecesInfo>();
      json['piecesInfo'].forEach((v) {
        piecesInfo.add(new PiecesInfo.fromJson(v));
      });
    }
    status = json['status'];
    fileName = json['fileName'];
    lastModify = json['lastModify'];
    numPieces = json['numPieces'];
    description = json['description'];
    totalBytes = json['totalBytes'];
    dirPath = json['dirPath'];
    id = json['id'];
    event = json['event'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numFailed'] = this.numFailed;
    if (this.piecesInfo != null) {
      data['piecesInfo'] = this.piecesInfo.map((v) => v.toJson()).toList();
    }
    data['fileName'] = this.fileName;
    data['lastModify'] = this.lastModify;
    data['numPieces'] = this.numPieces;
    data['description'] = this.description;
    data['totalBytes'] = this.totalBytes;
    data['dirPath'] = this.dirPath;
    data['id'] = this.id;
    data['event'] = this.event;
    data['url'] = this.url;
    return data;
  }
}

class PiecesInfo {
  String infoId;
  int size;
  int index;
  int curBytes;
  int speed;

  PiecesInfo({this.infoId, this.size, this.index, this.curBytes, this.speed});

  PiecesInfo.fromJson(Map<String, dynamic> json) {
    infoId = json['infoId'];
    size = json['size'];
    index = json['index'];
    curBytes = json['curBytes'];
    speed = json['speed'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['infoId'] = this.infoId;
    data['size'] = this.size;
    data['index'] = this.index;
    data['curBytes'] = this.curBytes;
    data['speed'] = this.speed;
    return data;
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_advanced_downloader_example/core/method_channel.dart';
import 'package:flutter_advanced_downloader_example/models/download_status_model.dart';

class DownloadManagerProvider extends ChangeNotifier{
  List<DownloadStatusModel> allDownloads;
  // List<DownloadStatusModel> completedDownloads;
  // List<DownloadStatusModel> notCompletedDownloads;
  StreamController _controller;

  DownloadManagerProvider(){
    _initStreamController();
    allDownloads = List();
  }

  getAllDownloads()async{
    var res = await MethodChannelHandler().getAllDownloadsPieceAndInfo();
    print (res);
  }

  deleteDownloadByID(String id,{bool withFile = true})async{
      MethodChannelHandler().deleteDownloadInfoById(id, withFile);
      allDownloads.removeWhere((element) => element.id == id);
      notifyListeners();
  }

  pauseOrResumeDownload(String id) async{
    await MethodChannelHandler().resumeOrPauseDownloadByID(id);
    //Stream will update it
  }

  pauseAll() async =>
  //stream will update it
      await MethodChannelHandler().pauseAllDownloads();

  stopAll() async =>
      //stream will update it
  await MethodChannelHandler().stopAllDownloads();


  addNewDownload(String url,{String savePath}) async{
    await MethodChannelHandler().startDownloadService(url: url,savePath: savePath);
    //Stream will update ui
  }

  void _initStreamController() async{
    _controller = StreamController.broadcast(
        onListen: ()=>print("onStreamListen called :/"),
        onCancel: ()=>_controller.close(),
        sync: false
    )
        ..stream.listen((data) {
          try {
            if (data != null) {
              var parsed = DownloadStatusModel.fromJson(jsonDecode(data));
              var available = allDownloads.firstWhere(
                      (ongoing) => ongoing.id == parsed.id,
                  orElse: () => null);
              if (available == null) {
                allDownloads.add(parsed);
              } else {
                allDownloads[allDownloads.indexOf(available)].piecesInfo =
                    parsed.piecesInfo;
              }
            }
            print("Updated State");
            notifyListeners();
          }catch(e){
            print("some error occurred while parsing stream data, : $e");
          }
    });

    await MethodChannelHandler().registerListener(_controller);

  }

}
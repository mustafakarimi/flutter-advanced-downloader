#import "FlutterAdvancedDownloaderPlugin.h"
#if __has_include(<flutter_advanced_downloader/flutter_advanced_downloader-Swift.h>)
#import <flutter_advanced_downloader/flutter_advanced_downloader-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_advanced_downloader-Swift.h"
#endif

@implementation FlutterAdvancedDownloaderPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterAdvancedDownloaderPlugin registerWithRegistrar:registrar];
}
@end

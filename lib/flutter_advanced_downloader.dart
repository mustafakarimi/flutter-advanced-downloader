
import 'dart:async';

import 'package:flutter/services.dart';

class FlutterAdvancedDownloader {
  static const MethodChannel _channel =
      const MethodChannel('flutter_advanced_downloader');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}

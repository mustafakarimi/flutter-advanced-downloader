package org.letuscode.flutter_advanced_downloader;

import android.os.Handler;
import android.os.Looper;

import org.json.JSONObject;

import java.util.HashMap;

import io.flutter.plugin.common.EventChannel;


public  class HandlerSingleton
{
    // static variable single_instance of type Singleton
    private static HandlerSingleton single_instance = null;
    static EventChannel.EventSink sink;
    private Handler handler = new Handler(Looper.getMainLooper());

    // private constructor restricted to this class itself
    private HandlerSingleton()
    {
    }

    // static method to create instance of Singleton class
    public static HandlerSingleton getInstance()
    {
        if (single_instance == null)
            single_instance = new HandlerSingleton();

        return single_instance;
    }

    public void setSink(EventChannel.EventSink sinkData) {
        sink = sinkData;
    }
    public void streamData(HashMap status) {
        if (sink != null) {
            System.out.println("Data Streamed");
            // handle in ui thread
            handler.post(() -> {
                sink.success(new JSONObject(status).toString());
            });
        }
    }

    public void streamError(String error) {
        if(sink != null) {
            handler.post(() -> {
                sink.error("error",error, null);
            });
        }
    }
}
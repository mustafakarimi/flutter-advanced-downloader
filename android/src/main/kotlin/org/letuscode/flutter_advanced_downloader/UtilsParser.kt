package com.tetadownloader.app.tetadownload

import org.letuscode.flutter_advanced_downloader.core.model.data.entity.DownloadInfo
import org.letuscode.flutter_advanced_downloader.core.model.data.entity.InfoAndPieces
import org.json.JSONObject


class UtilsParser {
    public fun infoToJson(eventName: String,info: InfoAndPieces): HashMap<String, Any> {
        var data = HashMap<String, Any>();
        data.put("event",eventName)
        data.put("description",info.info.description ?: "")
        data.put("dirPath",info.info.dirPath?.toString() ?: "")
        data.put("fileName",info?.info?.fileName!!)
        data.put("id",info?.info?.id!!)
        data.put("lastModify",info?.info?.lastModify!!)
        data.put("numFailed",info?.info?.numFailed!!)
        data.put("numPieces",info?.info?.numPieces!!)
        data.put("totalBytes",info?.info?.totalBytes!!)
        data.put("url",info?.info?.url!!)
        data.put("status",info?.info?.statusMsg ?: "")

        val piecesInfo : ArrayList<HashMap<String, Any>> = ArrayList()
        for (pieceData in info.pieces) {
            var pieceInfoModel = HashMap<String, Any>();
            pieceInfoModel["curBytes"] = pieceData?.curBytes!!;
            pieceInfoModel["index"] = pieceData?.index!!;
            pieceInfoModel["infoId"] = pieceData?.infoId?.toString()!!;
            pieceInfoModel["size"] = pieceData?.size!!;
            pieceInfoModel["speed"] = pieceData?.speed!!;
            piecesInfo.add(pieceInfoModel)
        }

        data.put("piecesInfo", piecesInfo);
        return data;

    }

    public fun listOfDownloadInfoToJson(infoList: MutableList<DownloadInfo>?): String {
        var result = HashMap<String, Any>();

        val downloadInfo : ArrayList<HashMap<String, Any>> = ArrayList()
        if (infoList != null) {
            for (info in infoList) {
                var data = HashMap<String, Any>();
                data.put("description", info?.description?.toString() ?: "null")
                data.put("dirPath", info?.dirPath?.toString() ?: "")
                data.put("fileName", info?.fileName!!)
                data.put("id", info?.id!!)
                data.put("lastModify", info?.lastModify!!)
                data.put("numFailed", info?.numFailed!!)
                data.put("numPieces", info?.numPieces!!)
                data.put("totalBytes", info?.totalBytes!!)
                data.put("url", info?.url!!)
                data.put("status", info?.statusMsg?.toString() ?: "null")
                downloadInfo.add(data)
            }
        }
        result.put("info",downloadInfo);

        return JSONObject(result as Map<*, *>).toString();

    }

    public fun singleDownloadInfoToJson(info: DownloadInfo?): String {
        var data = HashMap<String, Any>();

        if (info != null) {
                data.put("description", info?.description?.toString() ?: "null")
                data.put("dirPath", info?.dirPath?.toString() ?: "")
                data.put("fileName", info?.fileName!!)
                data.put("id", info?.id!!)
                data.put("lastModify", info?.lastModify!!)
                data.put("numFailed", info?.numFailed!!)
                data.put("numPieces", info?.numPieces!!)
                data.put("totalBytes", info?.totalBytes!!)
                data.put("url", info?.url!!)
                data.put("status", info?.statusMsg?.toString() ?: "null")
            }


        return JSONObject(data as Map<*, *>).toString();

    }

}
package org.letuscode.flutter_advanced_downloader

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import android.text.TextUtils
import androidx.databinding.ObservableInt
import com.tetadownloader.app.tetadownload.UtilsParser
import org.letuscode.flutter_advanced_downloader.core.DownloadNotifier
import org.letuscode.flutter_advanced_downloader.core.HttpConnection
import org.letuscode.flutter_advanced_downloader.core.RepositoryHelper
import org.letuscode.flutter_advanced_downloader.core.exception.HttpException
import org.letuscode.flutter_advanced_downloader.core.exception.NormalizeUrlException
import org.letuscode.flutter_advanced_downloader.core.model.ChangeableParams
import org.letuscode.flutter_advanced_downloader.core.model.DownloadEngine
import org.letuscode.flutter_advanced_downloader.core.model.DownloadEngineListener
import org.letuscode.flutter_advanced_downloader.core.model.data.entity.DownloadInfo
import org.letuscode.flutter_advanced_downloader.core.model.data.entity.Header
import org.letuscode.flutter_advanced_downloader.core.model.data.entity.InfoAndPieces
import org.letuscode.flutter_advanced_downloader.core.settings.SettingsRepository
import org.letuscode.flutter_advanced_downloader.core.storage.DataRepository
import org.letuscode.flutter_advanced_downloader.core.system.FileSystemFacade
import org.letuscode.flutter_advanced_downloader.core.system.SystemFacadeHelper
import org.letuscode.flutter_advanced_downloader.core.urlnormalizer.NormalizeUrl
import org.letuscode.flutter_advanced_downloader.core.utils.MimeTypeUtils
import org.letuscode.flutter_advanced_downloader.core.utils.Utils
import io.flutter.app.FlutterApplication
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import java.io.FileNotFoundException
import java.io.IOException
import java.net.HttpURLConnection
import java.util.*
import kotlin.collections.ArrayList
/** FlutterAdvancedDownloaderPlugin */
class FlutterAdvancedDownloaderPlugin: FlutterActivity(), FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  val METHODCHANNEL = "downloader/method_call";
  val EVENTCHANNEL = "downloader/event_call";
  var viewModel: AddDownloadViewModel? = null;
  private var repo: DataRepository? = null

  var params:AddDownloadParams = AddDownloadParams()
  var pref: SettingsRepository? = null
  var fs: FileSystemFacade? = null
  var maxNumPieces = ObservableInt(DownloadInfo.MAX_PIECES)
  private var engine: DownloadEngine? = null
  //    var downloadViewModel: DownloadsViewModel ? = null;
  private var handler: Handler? = Handler(Looper.getMainLooper())
  var result:MethodChannel.Result? = null


  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {

    println("Starting downloader service")
    // start downloader service
    val downloadNotifier = DownloadNotifier.getInstance(FlutterApplication().baseContext)
    downloadNotifier.makeNotifyChans()
    downloadNotifier.startUpdate()


    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_advanced_downloader")
    channel.setMethodCallHandler(this)

    repo = RepositoryHelper.getDataRepository(flutterPluginBinding.applicationContext)

    pref = RepositoryHelper.getSettingsRepository(flutterPluginBinding.applicationContext)
    fs = SystemFacadeHelper.getFileSystemFacade(flutterPluginBinding.applicationContext)
    engine = DownloadEngine.getInstance(flutterPluginBinding.applicationContext)

    EventChannel(flutterPluginBinding.binaryMessenger, EVENTCHANNEL).setStreamHandler(object : EventChannel.StreamHandler {
      override fun onListen(arguments: Any?, events: EventChannel.EventSink) {
        runOnUiThread {
          HandlerSingleton.getInstance().setSink(events);
        }
        events.success("Event Recieved")
      }

      override fun onCancel(arguments: Any?) {
        println("Closing handlers")
      }
    });

    MethodChannel(flutterPluginBinding.binaryMessenger, METHODCHANNEL).setMethodCallHandler { call, result ->
      this.result = result
      if (call.method == "start_download") {
        var url = call.argument<String>("url");
        var savePath = call.argument<String>("path")
        var _parsedUri = Uri.parse(savePath)
        println("start download called")
        startDownloadService(url!!, _parsedUri)
      }else if (call.method == "register_listener") {
        downloaderListener();
      }else if (call.method == "getAllDownloadsInfo") {
        getAllDownloadsInfo();
      }else if(call.method =="getDownloadInfoById"){
        getDownloadInfoById(call.argument<String>("id"))
      }else if(call.method == "deleteDownloadInfoById"){
        deleteDownloadInfoById(call.argument<String>("id"),call.argument<Boolean>("withFile"))
      }else if(call.method == "pauseAllDownloads"){
        pauseAllDownloads()
      }else if(call.method == "stopAllDownloads"){
        stopAllDownloads()
      }else if(call.method == "resumeOrPauseDownloadByID"){
        resumeOrPauseDownloadByID(call.argument<String>("id"))
      }else if (call.method == "change_base_url") {
        var uuid = call.argument<String>("uuid");
        var url = call.argument<String>("url");
        if (url != null && uuid != null) {
          changeDownloadBaseUrl(uuid, url);
        }
      }else if (call.method == "getAllDownloadsPieceAndInfo") {
        getAllDownloadsPieceAndInfo()
      }

    }


  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }



  /////// Downlaoder Functions

  /* Start OF Declaring New function */

  private fun getAllDownloadsPieceAndInfo(): HashMap<String, Any>?{
    repo?.allInfoAndPiecesSingle
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ data->
              val list : ArrayList<HashMap<String, Any>> = ArrayList()

              for(info in data) {
                list.add(
                        UtilsParser().infoToJson("progress",info)
                )
              }
              var result = HashMap<String, Any>();
              result.put("infoAndPiecesList",list);
              handler!!.post {
                this?.result?.success(JSONObject(result as Map<*, *>).toString())
              }
            },{
              it.printStackTrace()
            })

    return null
  }

  private fun getAllDownloadsInfo(): HashMap<String, Any>?{
    engine?.reschedulePendingDownloads()
    Single.fromCallable {
      // handle in ui thread
      (repo?.allInfo)
    }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ data ->
              handler!!.post {
                this.result?.success(UtilsParser().listOfDownloadInfoToJson(data))
              }
            }, {
              it.printStackTrace()
            })
    return null
  }

  private fun getDownloadInfoById(id: String?): DownloadInfo?{
    return if(id == null)
      null
    else {
      val uuid: UUID = UUID.fromString(id)
      Single.fromCallable {
        // handle in ui thread
        repo?.getInfoById(uuid)
      }
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe({ data ->
                handler!!.post {
                  this.result?.success(UtilsParser().singleDownloadInfoToJson(data))
                }
              }, {
                it.printStackTrace()
              })
      return null
    }
  }

  private fun deleteDownloadInfoById(id: String?, withFile: Boolean?){
    if(id != null) {

      val uuid: UUID = UUID.fromString(id)
      Single.fromCallable {
        // handle in ui thread
        val info:DownloadInfo? = repo?.getInfoById(uuid)
        if (info != null) {
          engine?.deleteDownloads(withFile?:false,info)
        }
      }
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .doOnError {
                handler!!.post {
                  this.result?.error("-1","could not delete item","");
                }
              }
              .subscribe({ data ->
                handler!!.post {
                  this.result?.success(null)
                }
              }, {
                it.printStackTrace()
              })

    }
  }

  private fun pauseAllDownloads(){
    engine?.pauseAllDownloads();
  }

  fun changeDownloadBaseUrl(id: String, newUrl: String) {
    var uuid = UUID.fromString(id);
    var changingParams = ChangeableParams();
    changingParams.url = newUrl;
    changingParams.unmeteredConnectionsOnly = false;
    engine?.changeParams(uuid, changingParams)
  }

  private fun stopAllDownloads(){
    engine?.stopDownloads();
  }

  private fun resumeOrPauseDownloadByID(id: String?){
    if(id != null) {
      val uuid: UUID = UUID.fromString(id)
      engine?.pauseResumeDownload(uuid)
    }
  }

  /* end OF Declaring New function */


  private fun startDownloadService(url: String, dirPath: Uri){
    if (params != null) {
      params = AddDownloadParams();
      doCheckFile(url, dirPath)
    };
  }


  @Throws(NormalizeUrlException::class)
  private fun makeDownloadInfo(dirPath: Uri): DownloadInfo? {
//        val state: FetchState = fetchState.getValue()
    var url = params.url
    params.dirPath = dirPath;
//        if (state != null && state.status != AddDownloadViewModel.Status.FETCHED) url = NormalizeUrl.normalize(url)
    val filePath = fs!!.getFileUri(params.dirPath, params.fileName)
    var fileName = params.fileName
    if (!fs!!.isValidFatFilename(fileName)) fileName = fs!!.buildValidFatFilename(params.fileName)
    fileName = fs!!.appendExtension(fileName!!, params.mimeType)
    if (params.isReplaceFile) {
      try {
        if (filePath != null) fs!!.deleteFile(filePath)
      } catch (e: FileNotFoundException) {
        /* Ignore */
      } catch (e: IllegalArgumentException) {
      }
    } else {
      fileName = fs!!.makeFilename(params.dirPath, fileName)
    }
    val info = DownloadInfo(dirPath, url!!, fileName)
    info.mimeType = params.mimeType
    info.totalBytes = params.totalBytes
    info.description = params.description
    info.unmeteredConnectionsOnly = params.isUnmeteredConnectionsOnly
    info.partialSupport = params.isPartialSupport
    info.numPieces = if (params.isPartialSupport && params.totalBytes > 0) params.numPieces else DownloadInfo.MIN_PIECES
    info.retry = params.isRetry
    info.userAgent = params.userAgent
    val checksum = params.checksum
//        if (isChecksumValid(checksum)) info.checksum = checksum
    info.dateAdded = System.currentTimeMillis()
//        if (state != null) info.hasMetadata = state.status == AddDownloadViewModel.Status.FETCHED
    return info
  }


  private fun doCheckFile(url: String, dirPath: Uri): Exception? {
    Single.fromCallable {

      val connection: HttpConnection
      connection = try {
        HttpConnection(url)
      } catch (e: Exception) {
        throw e;
      }
      connection.setTimeout(10000)

      val err = arrayOfNulls<Exception>(1)
      connection.setListener(@SuppressLint("CheckResult")
      object : HttpConnection.Listener {
        override fun onConnectionCreated(conn: HttpURLConnection) {
          /* TODO: maybe user agent spoofing (from settings) */
        }

        override fun onResponseHandle(conn: HttpURLConnection, code: Int, message: String) {
          if (code == HttpURLConnection.HTTP_OK) {
            println("Http Connection Success")
            parseHeaders(conn, dirPath)
          } else err[0] = HttpException("Failed to fetch link, response code: $code", code)
        }

        override fun onMovedPermanently(newUrl: String) {
          try {
            var url = NormalizeUrl.normalize(newUrl);
//                    viewModel.get().params.setUrl(NormalizeUrl.normalize(newUrl))
          } catch (e: NormalizeUrlException) {
            err[0] = e
          }
        }

        override fun onIOException(e: IOException) {
          err[0] = e
        }

        override fun onTooManyRedirects() {
          err[0] = HttpException("Too many redirects")
        }
      })
      connection.run();
    }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ text ->

            }, {
              it.printStackTrace()
            })
    return null;
  }

  public fun parseHeaders(conn: HttpURLConnection, downloadPath: Uri) {
    val contentDisposition = conn.getHeaderField("Content-Disposition")
    val contentLocation = conn.getHeaderField("Content-Location")
    val tmpUrl = conn.url.toString()
    params.url = conn.url.toString()
    var mimeType = Intent.normalizeMimeType(conn.contentType)
    /* Try to determine the MIME type later by the filename extension */if ("application/octet-stream" == mimeType) mimeType = null
    if (TextUtils.isEmpty(params.fileName)) params.fileName = fs?.let {
      Utils.getHttpFileName(it,
              tmpUrl,
              contentDisposition,
              contentLocation,
              mimeType)
    }

    /* Try to get MIME from filename extension */
    if (mimeType == null) {
      val extension: String? = fs?.getExtension(params.fileName)
      if (!TextUtils.isEmpty(extension)) mimeType = MimeTypeUtils.getMimeTypeFromExtension(extension)
    }
    println(params.fileName);
    if (mimeType != null) params.mimeType = mimeType
    params.etag = conn.getHeaderField("ETag")
    val transferEncoding = conn.getHeaderField("Transfer-Encoding")
    if (transferEncoding == null) {
      try {
        params.totalBytes = conn.getHeaderField("Content-Length").toLong()
      } catch (e: NumberFormatException) {
        params.totalBytes = -1
      }
    } else {
      params.totalBytes = -1
    }
    params.isPartialSupport = "bytes".equals(conn.getHeaderField("Accept-Ranges"), ignoreCase = true)

    /* The number of pieces can't be more than the number of bytes */
    val total = params.totalBytes
    if (total > 0) maxNumPieces.set(if (total < maxNumPieces.get()) total.toInt() else DownloadInfo.MAX_PIECES);
    val headers = ArrayList<Header>()
    var _info = makeDownloadInfo(downloadPath);
    if (_info != null) {
      /* TODO: rewrite to WorkManager */
      /* Sync wait inserting */try {
        val t = Thread(Runnable { if (pref?.replaceDuplicateDownloads()!!) repo!!.replaceInfoByUrl(_info, headers) else repo!!.addInfo(_info, headers) })
        t.start()
        t.join()
      } catch (e: InterruptedException) {
        return
      }
      engine?.runDownload(_info)
    }else {
      throw NullPointerException()
    }
  }

  public  fun downloaderListener() {
    if (repo != null) {
      repo?.observeAllInfoAndPieces()
              ?.subscribeOn(Schedulers.io())
              ?.flatMapSingle({ infoAndPiecesList ->
                Flowable.fromIterable(infoAndPiecesList)
                        .toList()
              }
              )
              ?.observeOn(AndroidSchedulers.mainThread())
              ?.subscribe({ data ->
                var infos = data as ArrayList<Any>;
                for (info in infos) {
                  var dataModel = info as InfoAndPieces;
                  if (dataModel != null) {
                    var model = UtilsParser().infoToJson("progress",dataModel);
                    HandlerSingleton.getInstance().streamData(model);
                  }
                }
              }, {
                it.printStackTrace()
              })
    }
  }



}
